<!--
 * @Author: Edwin
 * @Date: 2022-05-16 21:27:22
 * @LastEditTime: 2022-05-16 21:28:00
 * @LastEditors: Edwin
 * @Description: 
-->
## YADANSoC

### 介绍
这是基于YADAN Core设计的SoC，可以使用Arduino IDE进行设计。SoC的结构如下：

![picture 2](./images/066967b967054565b059580ce47caf936e92cf6183aac0258c37272da604d6ef.png)  

内存地址映射如下：

![picture 3](./images/3cb5dafbf895242aa56cc9f802571043757213197c754f30161626ba4bf115d6.png)  

### 使用方法
1、该工程在YADAN BOARD开发板上实现，使用安路科技的EG4SNG88FPGA作为主控。

![picture 4](./images/a403f40185422bb9b7d0ed50de233179671f858c4d0f7753c559675f54f49210.png)  

2、该工程的目录结构如下：

![picture 5](./images/903b65e78fd6e5a2c4c304f0d897f07afb7e24d3a64290564498d3bab4e8bc68.png)  

其中prj里存放了安路TangDynasty工具建立的工程文件。RTL是内核和外设源码，内核源码由Verilog编写，外设来自于Opencores网站的开源外设方案。也可以自行设计进行替换。

3、配置bit流文件到FPGA中
打开工程目录prj，里面的文件如下：

![picture 6](./images/672dc6a3a42829141630810e02555e00a6d9b40591a62c7b2e0875bbc81ccd60.png)  

其中Yadan.al为TangDynasty工程文件，Yadan.adc为管脚约束文件，Yadan.sdc为时序约束文件；Yadan.bit为配置FPGA的bit流文件。

双击Yadan.al打开工程，（前提是安装好TangDynasty工具，本工程使用的TangDynasty版本号为win版本5.0.3 29524）

![picture 7](./images/7a18290be7eb37daf6537dc3e2bef1723748c19d693382c952dd6632fa07dacb.png)  

选择下载bit流工具![picture 8](./images/eb12bfb6609d573f5ad6b86196a0a40b64bc55f0fc548a5702d8552e5bf5aac2.png) ，打开下载工具。 

![picture 9](./images/e627933181cceac35a7694e4822a93750a8574b8275dcba9e690d81f5a11b6f4.png)  

将Yadan_Board通过TPYE-C的usb线和PC机连接，在No hardware选择中就会自动加载为EG4S20NG88，（如果加载不了需要检查下载器驱动是否安装，驱动在TangDynasty的安装目录下。）

![picture 10](./images/5d15cabfb44dae15637e17428947f388e8209d21fdf536f7386906511eb31b2a.png)  

加载Yadan_Board成功后，我们需要将Yadan.bit下载到开发板，这里有两种常用模式，JTAG和PROGRAM FLASH，JTAG模式下下载速度较快，但是开发板断电重新上电后需要重新下载bit文件，PROGRAM FLASH模式是将bit文件下载到开发板上的flash中，开发板断电后重新上电fpga会从flash重新加载bit，不需要再从pc下载bit。

点击ADD ![picture 12](./images/f64498113afc070f57fd7cf8db976fe0c728ac301194c93d464f0d4acd8c2f02.png)  
选择需要下载的Yadan_core.bit文件，如下图

![picture 13](./images/22d372d0e35f23d5b01b6abe0867ffe3da2b796942e821d369aa642e54bd09e4.png)  

此时的Run从灰色变成了绿色。根据需求就可以设置下载Mode模式，点击Run就可以下载了。

这里需要注意，PROGRAM FLASH模式需要将bit下载到Yadan_board上flash中，下载速度较慢，所以建议在调试阶段使用JTAG模式，当开发完成时使用PROGRAM FLASH模式。此外在使用PROGRAM FLASH模式的时候建议只插入TYPE-C的usb口到PC机。将串口的usb口断开。

4、使用Arduino IDE开发
详细的方法参见：https://gitee.com/verimake/yadan_arduino

### 后续计划
1、更新中断控制器Event/Interrupt Unit。

2、更新I2C外设。

3、添加GNU工具链裸机开发教程。